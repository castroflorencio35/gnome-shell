/* -*- mode: C; c-file-style: "gnu"; indent-tabs-mode: nil; -*- */

#ifndef __SHELL_VR_MIRROR_H__
#define __SHELL_VR_MIRROR_H__

#include <clutter/clutter.h>
#include "st.h"

#include "shell-global.h"
#include <xrd.h>

/* TODO: generate enum header with gschemas */
typedef enum {
  UPLOAD_METHOD_VK_SHARED_GL_MEMORY = 1,
  UPLOAD_METHOD_VK_UPLOAD_RAW
} UploadMethod;

G_BEGIN_DECLS

#define SHELL_TYPE_VR_MIRROR (shell_vr_mirror_get_type ())
G_DECLARE_FINAL_TYPE (ShellVRMirror, shell_vr_mirror,
                      SHELL, VR_MIRROR, GObject)

ShellVRMirror *shell_vr_mirror_new          (void);
ShellVRMirror *shell_vr_mirror_get_instance (void);
ShellVRMirror *shell_vr_mirror_create_instance (void);
void shell_vr_mirror_destroy_instance (void);

void shell_vr_mirror_initialize (ShellVRMirror *self);

gboolean
shell_vr_mirror_map_actor (ShellVRMirror   *self,
                           MetaWindowActor *actor);

gboolean
shell_vr_mirror_destroy_actor (ShellVRMirror   *self,
                               MetaWindowActor *actor);

gboolean
shell_vr_mirror_actor_size_changed (ShellVRMirror   *self,
                                    MetaWindowActor *actor);

void
shell_dbus_xr_init (void);

G_END_DECLS

#endif /* __SHELL_VR_MIRROR_H__ */
